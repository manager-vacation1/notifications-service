package com.dharbor.talent.managervacations.notificationsservice.service;

import com.dharbor.talent.managervacations.notificationsservice.domain.Notification;
import com.dharbor.talent.managervacations.notificationsservice.repository.NotificationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class NotificationServiceImplTest {

    @Mock
    private NotificationRepository notificationRepository;

    private NotificationServiceImpl undertest;

    @BeforeEach
    void setUp(){
        undertest = new NotificationServiceImpl(notificationRepository);
    }

    @Test
    void save() {
        Notification notificationToSave = new Notification();
        notificationToSave.setDayOff(new Date());
        notificationToSave.setIdManager(1L);
        notificationToSave.setIdUser(1L);
        notificationToSave.setIdVacation(1L);

        Notification notificationSaved = new Notification();
        notificationSaved.setIdVacation(notificationToSave.getIdVacation());
        notificationSaved.setIdUser(notificationToSave.getIdUser());
        notificationSaved.setIdManager(notificationToSave.getIdManager());
        notificationSaved.setDayOff(notificationToSave.getDayOff());
        notificationSaved.setId(1L);

        Mockito.when(notificationRepository.save(notificationToSave)).thenReturn(notificationSaved);
        Notification result = undertest.save(notificationToSave);

        verify(notificationRepository, times(1)).save(notificationToSave);
        assertTrue(result.getId().compareTo(1L) == 0);

    }
}