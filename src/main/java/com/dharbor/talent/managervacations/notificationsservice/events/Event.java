package com.dharbor.talent.managervacations.notificationsservice.events;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Data
@ToString
public abstract class Event<T> {
    private String id;
    private Date date;
    private EventType eventType;
    private T data;
}
