package com.dharbor.talent.managervacations.notificationsservice.controller;

import com.dharbor.talent.managervacations.notificationsservice.dto.response.NotificationResponse;
import com.dharbor.talent.managervacations.notificationsservice.usecase.GetNotificationIdManagerUseCase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jhonatan Soto
 */
@RestController
@RequestMapping("/notifications")
@AllArgsConstructor
public class NotificationController {

    private GetNotificationIdManagerUseCase getNotificationIdManagerUseCase;

    @GetMapping("/{userId}")
    public NotificationResponse getListNotifications(@PathVariable Long userId) {
        System.out.println("hoal");
        return getNotificationIdManagerUseCase.execute(userId);
    }

}
