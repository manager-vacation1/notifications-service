package com.dharbor.talent.managervacations.notificationsservice.mapper;

import com.dharbor.talent.managervacations.notificationsservice.domain.Notification;
import com.dharbor.talent.managervacations.notificationsservice.dto.NotificationDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface NotificationStructMapper {

    List<NotificationDto> notificationListToNotificationListDto(List<Notification> list);

}
