package com.dharbor.talent.managervacations.notificationsservice.dto.response;

import com.dharbor.talent.managervacations.notificationsservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.notificationsservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.notificationsservice.dto.NotificationDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class NotificationResponse extends CommonResponseDomain {
    private List<NotificationDto> vacationDtos;

    public NotificationResponse(List<NotificationDto> vacationDtos) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.vacationDtos = vacationDtos;
    }
}
