package com.dharbor.talent.managervacations.notificationsservice.constant;

/**
 * @author Jhonatan Soto
 */
public enum VacationType {
    HOLIDAY,
    COMPENSATION,
    DENIED;

    public static boolean findByType(VacationType vacationType) {
        for (VacationType v : VacationType.values()) {
            if(v.equals(vacationType)){
                return true;
            }
        }
        return false;
    }
}
