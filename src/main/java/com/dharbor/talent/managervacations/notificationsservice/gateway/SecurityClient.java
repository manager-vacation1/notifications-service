package com.dharbor.talent.managervacations.notificationsservice.gateway;

import com.dharbor.talent.managervacations.notificationsservice.gateway.dto.response.UserResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Jhonatan Soto
 */
@FeignClient("security-service")
public interface SecurityClient {
    @GetMapping("profile/get/manager/{userId}")
    UserResponse getManagerByUserId(@PathVariable Long userId);
}
