package com.dharbor.talent.managervacations.notificationsservice.constant;

public enum UserType {
    MANAGER, COLLABORATOR;
}